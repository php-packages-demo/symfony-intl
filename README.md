# symfony/intl

PHP replacement layer for the C intl extension that includes additional data from the ICU library. https://packagist.org/packages/symfony/intl

[![PHPPackages Rank](http://phppackages.org/p/symfony/intl/badge/rank.svg)](http://phppackages.org/p/symfony/intl)
[![PHPPackages Referenced By](http://phppackages.org/p/symfony/intl/badge/referenced-by.svg)](http://phppackages.org/p/symfony/intl)

## Unofficial documentation
### Internationalization
* [*Your Guide to Symfony 4 Internationalization Beyond the Basics*
  ](https://medium.com/@phrase/your-guide-to-symfony-4-internationalization-beyond-the-basics-122c6a708c8)
  2019-09 Phrase
